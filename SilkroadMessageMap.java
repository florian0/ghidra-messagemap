//Find all message maps based on the deepest message map CObjChild
//@author florian0
//@category _SRO
//@keybinding
//@menupath
//@toolbar

import ghidra.app.cmd.data.CreateDataCmd;
import ghidra.app.cmd.function.CreateFunctionCmd;
import ghidra.app.script.GhidraScript;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.model.util.*;
import ghidra.program.model.reloc.*;
import ghidra.program.model.data.*;
import ghidra.program.model.block.*;
import ghidra.program.model.symbol.*;
import ghidra.program.model.scalar.*;
import ghidra.program.model.mem.*;
import ghidra.program.model.listing.*;
import ghidra.program.model.lang.*;
import ghidra.program.model.pcode.*;
import ghidra.program.model.address.*;
import ghidra.program.model.correlate.*;
import ghidra.util.exception.DuplicateNameException;
import ghidra.util.exception.InvalidInputException;

import java.util.ArrayList;
import java.util.HashSet;


public class SilkroadMessageMap extends GhidraScript {

    int functionCount = 0;
    int labelCount = 0;


    // This is pure gore. Sorry. Need to reserve a place in hell.
    class ProgramFlowControlException extends Exception {
        public ProgramFlowControlException(String msg) {
            super(msg);
        }
    }

    class DuplicateFunctionException extends ProgramFlowControlException {
        public DuplicateFunctionException(String msg) {
            super(msg);
        }
    }


    class MessageMapEntry {

        MessageMapEntry(Address startingAddress) throws MemoryAccessException, Exception {

            m_startingAddress = startingAddress;
            m_nCommand = getInt(startingAddress);
            m_idFrom = getInt(startingAddress.add(8));
            m_idTo = getInt(startingAddress.add(12));


            // Offset of fun ptr
            // a0 - 88 = 0x18
            Address entryFn = startingAddress.add(0x18);

            // Check if we only got one reference, major error otherwise
            if (currentProgram.getReferenceManager().getReferenceCountTo(entryFn) != 1) {
                // unusable entry ...
                throw new Exception("Unusable entry at " + entryFn.toString());
            }

            var ref = getReferencesTo(entryFn)[0];

            Instruction instruction = getInstructionAt(ref.getFromAddress());

            if (instruction.getMnemonicString().compareToIgnoreCase("mov") != 0)
                throw new Exception("Failed to parse instruction " + instruction.toString() + " at " + instruction.getAddress().toString());


            String operandToSearch = instruction.getDefaultOperandRepresentation(1);

            String value = findValueForOperand(instruction, operandToSearch);

            if (value.compareToIgnoreCase("0") != 0) {
                Address handlerFnAddress = getAddressFactory().getAddress(value);

                if (currentProgram.getReferenceManager().getReferenceCountTo(handlerFnAddress) != 1)
                    throw new DuplicateFunctionException(handlerFnAddress.toString("Duplicate use of function "));

                m_handlerFnAddress = handlerFnAddress;
            }
        }

        // Create the handler function
        public void CreateHandlerFunction(Namespace namespace) throws Exception {
            Function function = getFunctionAt(m_handlerFnAddress);

            // Create the function if not existing
            if (function == null) {
                CreateFunctionCmd cmd = new CreateFunctionCmd(null, m_handlerFnAddress, null, SourceType.ANALYSIS);
                cmd.applyTo(currentProgram);

                function = getFunctionAt(m_handlerFnAddress);
            }

            // Build the name
            StringBuilder name = new StringBuilder();
            switch (m_nCommand) {
                case 1: // Click
                    name.append("OnClick_");

                    if (m_idFrom == m_idTo) {
                        name.append(m_idTo);
                    } else {
                        name.append(m_idFrom);
                        name.append("_");
                        name.append(m_idTo);
                    }
                    break;

                case 2: // Vertical Scroll
                    name.append("OnVScroll_");

                    if (m_idFrom == m_idTo) {
                        name.append(m_idTo);
                    } else {
                        name.append(m_idFrom);
                        name.append("_");
                        name.append(m_idTo);
                    }
                    break;

                case 258:
                    name.append("OnChar");
                    break;

                default:
                    name.append("On_CMD");
                    name.append(m_nCommand);
                    break;

            }

            labelAt(function, name.toString(), namespace);

            // TODO: Apply calling convention here!
            // TODO: Apply parameter types and names here!
        }

        public boolean isLast() {
            return m_handlerFnAddress == null;
        }

        public String toString() {
            StringBuilder output = new StringBuilder();

            output.append("MessageMapEntry[ ST ");
            output.append(m_startingAddress.toString());
            output.append(" FN ");
            output.append(m_handlerFnAddress.toString());
            output.append("]");

            return output.toString();
        }

        private int m_idFrom;
        private int m_idTo;
        private int m_nCommand;

        private Address m_startingAddress = null;
        private Address m_handlerFnAddress = null;
    }

    class MessageMap {

        public MessageMap(Address address) throws Exception {
            m_messageMap = address;

            // This gets the message map entries array by resolving the pointer in the message map
            // TODO: Make this load the offset (4) from the actual structure.
            m_messageMapEntries = address.getNewAddress(currentProgram.getMemory().getInt(address.add(4)));

            m_namespace = findNamespace();
        }

        public String toString() {
            StringBuilder output = new StringBuilder();
            output.append("MessageMap[");
            output.append(m_namespace.toString());
            output.append(" - ");
            output.append(m_messageMap.toString());
            output.append("]");

            return output.toString();
        }

        private void ApplyAll() throws Exception {

            labelAt(m_getMessageMap, "GetMessageMap", m_namespace);

            labelAt(m_messageMap, "messageMap", m_namespace);
            labelAt(m_messageMapEntries, "messageMapEntries", m_namespace);

            var entries = getEntries();
            for (var entry : entries) {
                entry.CreateHandlerFunction(m_namespace);
            }
        }

        private Namespace findNamespace() throws Exception {

            // 1. Find all references to message map
            var references = currentProgram.getReferenceManager().getReferencesTo(m_messageMap);

            // Iterate all references
            for (var reference : references) {
                Address fromAddress = reference.getFromAddress();

                // 2. Determine if the reference is in data or in code segment
                MemoryBlock memoryBlock = currentProgram.getMemory().getBlock(fromAddress);

                // We're only interested in code-references
                if (!memoryBlock.isExecute())
                    continue;

                // In code region
                Instruction instruction = currentProgram.getListing().getInstructionAt(fromAddress);

                if (instruction.getMnemonicString().compareToIgnoreCase("mov") != 0)
                    continue;

                if (instruction.getNumOperands() != 2)
                    continue;

                if (instruction.getDefaultOperandRepresentation(0).compareToIgnoreCase("eax") != 0)
                    continue;

                // Very likely to be the GetMessageMap function!

                // 2.1 figure out the name of the class we're in
                // We can not use the vftable_meta_ptr, since not every class implements a GetMessageMap function

                // For now, just use the Namespace of the function
                Function function = currentProgram.getFunctionManager().getFunctionContaining(fromAddress);
                Namespace namespace = function.getParentNamespace();

                if (namespace.isGlobal()) {
                    println("Namespace of " + fromAddress.toString() + " is global :/");

                    // Try to find the vftable
                    if (currentProgram.getReferenceManager().getReferenceCountTo(function.getEntryPoint()) != 1) {
                        printerr("Invalid number of references (!= 1).");
                        continue;
                    }

                    Address vftableref = currentProgram.getReferenceManager().getReferencesTo(function.getEntryPoint()).next().getFromAddress();
                    Address vtable = vftableref.subtract(3 * 4);
                    var symbol = currentProgram.getSymbolTable().getPrimarySymbol(vtable);
                    namespace = symbol.getParentNamespace();

                    if (namespace.isGlobal()) {
                        printerr("Tried to find vftable. Namespace is still global!");
                        continue;
                    }
                }

                // We land here if we either have the namespace already, or succesfully got it from analysis
                m_getMessageMap = function;
                return namespace;
            }

            throw new Exception("Unable to find namespace");
        }

        private ArrayList<MessageMapEntry> getEntries() throws MemoryAccessException, Exception {

            String value = "";

            ArrayList<MessageMapEntry> entries = new ArrayList<MessageMapEntry>();

            boolean bIsLast = false;

            Address entryAddress = m_messageMapEntries;

            // Size of one entry is: 0xc10 - 0xbe8 = 0x28
            // Parse all entries in the message map
            do {
                try {
                    MessageMapEntry entry = new MessageMapEntry(entryAddress);

                    if (!entry.isLast())
                        entries.add(entry);

                    bIsLast = entry.isLast();

                } catch (DuplicateFunctionException ex) {
                    printerr(ex.getMessage());
                } catch (MemoryAccessException ex) {
                    // This exception usually occurs when the list is empty
                    // In this case the first entry of the list is already the end of list marker with uninitialized members.
                    printerr("MessageMap at " + entryAddress.toString() + " might be empty: " + ex.getMessage());
                    break; // Force exit the loop since we might have an empty list here!
                }

                entryAddress = entryAddress.add(0x28);
            } while (!bIsLast);

            //ArrayDataType type = new ArrayDataType(typeMsgMapEntries, numberOfEntries, 0x28);

            //CreateDataCmd cmd = new CreateDataCmd(startingAddress, type);
            //cmd.applyTo(currentProgram);

            return entries;
        }


        Address m_messageMap;
        Address m_messageMapEntries;
        Function m_getMessageMap;
        Namespace m_namespace;
    }


    HashSet<Address> step1_get_all_message_maps(Address startAddress) {
        HashSet<Address> messageMapsOpen = new HashSet<Address>();
        HashSet<Address> messageMapsClosed = new HashSet<Address>();

        // Add starting address to list
        messageMapsOpen.add(startAddress);

        // Keep iterating through the list as long as we have addresses left
        while (!messageMapsOpen.isEmpty()) {
            Address openMessageMapAddress = messageMapsOpen.iterator().next();

            // Move the current address to the closed list
            messageMapsOpen.remove(openMessageMapAddress);
            messageMapsClosed.add(openMessageMapAddress);


            // 1. Find all references to message map
            var references = currentProgram.getReferenceManager().getReferencesTo(openMessageMapAddress);

            // Iterate all references
            for (var reference : references) {
                Address fromAddress = reference.getFromAddress();

                // 2. Determine if the reference is in data or in code segment
                MemoryBlock memoryBlock = currentProgram.getMemory().getBlock(fromAddress);

                // We're not interested in code-references
                if (memoryBlock.isExecute())
                    continue;

                // Add found address to list to parse it later on
                if (!messageMapsClosed.contains(fromAddress)) {
                    messageMapsOpen.add(fromAddress);
                }
            }
        }

        return messageMapsClosed;
    }


    void labelAt(Address addr, String name, Namespace ns) throws Exception {

        Symbol sym = currentProgram.getSymbolTable().getPrimarySymbol(addr);
        if (sym == null) {
            currentProgram.getSymbolTable().createLabel(addr, name, ns, SourceType.ANALYSIS);
        } else {
            if (sym.getSource() == SourceType.USER_DEFINED) {
                printerr("Skipping " + addr.toString() + " due to USER_DEFINED symbol");
                return;
            }

            sym.setNameAndNamespace(name, ns, SourceType.ANALYSIS);
        }
        labelCount++;
    }

    void labelAt(Function function, String name, Namespace ns) throws Exception {


        if (function.getSignatureSource() == SourceType.USER_DEFINED) {
            printerr("Skipping " + function.toString() + " due to USER_DEFINED signature");
            return;
        }

        if (function.getSymbol().getSource() == SourceType.USER_DEFINED) {
            printerr("Skipping " + function.toString() + " due to USER_DEFINED symbol");
            return;
        }

        function.setParentNamespace(ns);
        function.setName(name, SourceType.ANALYSIS);
        functionCount++;
    }


    private String findValueForOperand(Instruction startingInstruction, String operandToSearch) throws Exception {

        Instruction currentInstruction = startingInstruction;

        for (int i = 0; i < 100; i++, currentInstruction = currentInstruction.getPrevious()) {
            if (currentInstruction.getMnemonicString().compareToIgnoreCase("mov") == 0)
                if (currentInstruction.getDefaultOperandRepresentation(0).compareToIgnoreCase(operandToSearch) == 0)
                    return currentInstruction.getDefaultOperandRepresentation(1);

            if (currentInstruction.getMnemonicString().compareToIgnoreCase("xor") == 0)
                if (currentInstruction.getDefaultOperandRepresentation(0).compareToIgnoreCase(operandToSearch) == 0 &&
                        currentInstruction.getDefaultOperandRepresentation(1).compareToIgnoreCase(operandToSearch) == 0)
                    return "0";
        }

        throw new Exception("Could not find value for operand on " + startingInstruction.getAddress().toString());
    }

    public void run() throws Exception {
        Address messageMapCObjChild = currentProgram.getAddressFactory().getAddress("00e08e2c");

        var messageMaps = step1_get_all_message_maps(messageMapCObjChild);

        int i = 0;

        for (var messageMap : messageMaps) {

            MessageMap msgmap = new MessageMap(messageMap);

            msgmap.ApplyAll();
        }

        //return;
/*

        typeMsgMap = currentProgram.getDataTypeManager().getDataType("/sro_client.exe/BSLib/GFX_MSGMAP");
        if (typeMsgMap == null)
            throw new Exception("GFX_MSGMAP not found");

        typeMsgMapEntries = currentProgram.getDataTypeManager().getDataType("/sro_client.exe/BSLib/GFX_MSGMAP_ENTRY");
        if (typeMsgMapEntries == null)
            throw new Exception("GFX_MSGMAP_ENTRY not found");

        */


        StringBuilder result = new StringBuilder();
        result.append("Applied ");
        result.append(labelCount);
        result.append(" labels and ");
        result.append(functionCount);
        result.append(" functions.");


        println(result.toString());
    }

    private DataType typeMsgMap = null;
    private DataType typeMsgMapEntries = null;


}
