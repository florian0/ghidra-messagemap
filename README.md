# Ghidra Message Map

Simple script to automatically analyze a MessageMap structure quite similar to 
Message Maps in MFC:

* https://docs.microsoft.com/en-us/cpp/mfc/reference/message-maps-mfc?view=vs-2005
* https://docs.microsoft.com/en-us/cpp/mfc/reference/message-map-macros-mfc?view=vs-2005


![Demo Screenshot](screenshot.png)

## Features

* Finds all message maps based on the pointer to the *deepest* message map in the tree.
* Identifies the message map entries and the message map pointer
* Applies correct types to all identified pointers
* Identifies all handling functions and adds them to the namespace

## Limitations

* Can not deal with GetMessageMap-functions that appear in more than one vftable
* Does not create the required types, if non existing

## Add these structures to your database

```cpp
struct GFX_MSGMAP_ENTRY {
    uint nMessage;
    uint nCode;
    uint nID;
    uint nLastID;
    uint nSig;
    undefined4 field_0x14;
    void * pfnHandler;
    undefined4 field_0x1c;
    undefined4 field_0x20;
    undefined4 field_0x24;
};

struct GFX_MSGMAP {
    struct GFX_MSGMAP * field_0x0;
    struct GFX_MSGMAP_ENTRY * field_0x4;
};

```
